const gamePageContainerEle = document.getElementById("gamePageContainer");
const gameContainer = document.getElementById("game");
const HeaderContentsContainerEle = document.getElementById(
  "HeaderContentsContainer"
);
let backButtonEle = document.getElementById("backButton");
let bestScoreEle = document.getElementById("bestScore");
let clickCount = 0;
let cumilatedClickCount = 0;
let scoreDisplayEle = document.getElementById("scoreDisplay");
let gameOverEle = document.getElementById("gameOver");
let scoreNumberDisplayEle = document.getElementById("scoreNumberDisplay");
let playAgainEle = document.getElementById("playAgain");

if (JSON.parse(localStorage.getItem("bestScore") != null)) {
  bestScoreEle.textContent = JSON.parse(localStorage.getItem("bestScore"));
}

let topImagePath = "https://c.tenor.com/TJLNj0knOq8AAAAC/dbsbank-dbs.gif";

let gifsArray = [];
for (index = 1; index <= 12; index++) {
  gifsArray.push(index.toString() + ".gif");
}

// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want to research more
function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}

let idsAfterClickArray = [];
/* let shuffledColors = shuffle(COLORS); */

// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
function createDivsForEachElement1(myArray) {
  for (let ele of myArray) {
    // create a new div
    const newDiv = document.createElement("div");
    newDiv.classList.add("imgDiv");

    const imageEl = document.createElement("img");
    imageEl.id = ele.split(".")[0];
    imageEl.src = topImagePath;
    imageEl.classList.add("gifImage");
    imageEl.addEventListener("click", handleCardClick);
    newDiv.appendChild(imageEl);
    gameContainer.append(newDiv);
  }
}

function createDivsForEachElement2(myArray) {
  for (let ele of myArray) {
    // create a new div
    const newDiv = document.createElement("div");
    newDiv.classList.add("imgDiv");

    const imageEl = document.createElement("img");
    imageEl.id = Number(ele.split(".")[0]) + 12;
    imageEl.src = topImagePath;

    imageEl.classList.add("gifImage");
    imageEl.addEventListener("click", handleCardClick);
    newDiv.appendChild(imageEl);
    gameContainer.append(newDiv);
  }
}
let matchedPairs = 0;
let id1;
// TODO: Implement this function!
function handleCardClick(event) {
  clickCount += 1;
  cumilatedClickCount += 1;
  scoreDisplayEle.textContent = cumilatedClickCount;

  if (event.target.id <= 12) {
    document.getElementById(
      event.target.id
    ).src = `./gifs/${event.target.id}.gif`;
  } else {
    document.getElementById(event.target.id).src = `./gifs/${
      event.target.id - 12
    }.gif`;
  }

  if (clickCount == 1) {
    id1 = event.target.id;
    idsAfterClickArray.push(Number(id1));
    document.getElementById(id1).removeEventListener("click", handleCardClick);
  }
  if (clickCount == 2) {
    idsAfterClickArray.push(Number(event.target.id));

    if (id1 == event.target.id - 12 || event.target.id == id1 - 12) {
      matchedPairs += 1;
      const id2El = document.getElementById(event.target.id);
      id2El.removeEventListener("click", handleCardClick);
      idsAfterClickArray = [];
      clickCount = 0;

      if (matchedPairs == 12) {
        if (localStorage.getItem("bestScore") == null)
          localStorage.setItem(
            "bestScore",
            JSON.stringify(cumilatedClickCount)
          );
        else {
          if (
            JSON.parse(localStorage.getItem("bestScore")) > cumilatedClickCount
          ) {
            localStorage.setItem(
              "bestScore",
              JSON.stringify(cumilatedClickCount)
            );
          } else {
            bestScoreEle.textContent = JSON.parse(
              localStorage.getItem("bestScore")
            );
          }
        }
      }

      if (matchedPairs == 12) {
        HeaderContentsContainerEle.classList.add("hideGame");
        gamePageContainerEle.classList.add("hideGame");
        gameOverEle.style.display = "flex";
        gameOverEle.style.flexDirection = "column";
        gameOverEle.style.alignItems = "center";
        gameOverEle.style.justifyContent = "center";
        scoreNumberDisplayEle.textContent = cumilatedClickCount;

        playAgainEle.addEventListener("click", function () {
          location.reload();
        });
      }
    } else {
      document.getElementById(id1).addEventListener("click", handleCardClick);
      flipImage(idsAfterClickArray);
      idsAfterClickArray = [];
    }
  }
}

function flipImage(idsAfterClickArray) {
  const arg1 = idsAfterClickArray[0];
  const arg2 = idsAfterClickArray[1];
  document.getElementById(arg1).src = topImagePath;
  clickCount = 0;
  setTimeout(() => {
    document.getElementById(arg2).src = topImagePath;
  }, 1000);
}
// when the DOM loads

let shuffledGifsarray1 = shuffle(gifsArray);

function startGame() {
  let startBtnEle = document.getElementById("startButton");

  startBtnEle.addEventListener("click", function () {
    HeaderContentsContainerEle.style.display = "none";
    gamePageContainerEle.classList.remove("hideGame");
    gameContainer.classList.remove("hideGame");
    backButtonEle.classList.remove("hideGame");
    createElements();
  });
}

function displayHomePage() {
  backButtonEle.addEventListener("click", function () {
    location.reload();
  });
}

function createElements() {
  createDivsForEachElement1(shuffledGifsarray1);
  createDivsForEachElement2(shuffle(shuffledGifsarray1));
}
startGame();
displayHomePage();
